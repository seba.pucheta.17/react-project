for i in $( git diff --name-only --staged ); do
  if [[ $i == config/config.* ]]; then
    sopsProperty=$(jq '.sops' $i)
    if [ "${sopsProperty}" = "null" ]; then
      exit 1
    fi
  fi
done
