import React from 'react';

const Cars = React.lazy(() => import('./views/cars/Cars'));
const Car = React.lazy(() => import('./views/cars/Car'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/cars', exact: true,  name: 'Cars', component: Cars },
  { path: '/cars/:id', exact: true, name: 'Car Details', component: Car }
];

export default routes;
