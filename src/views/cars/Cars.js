import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CPagination
} from '@coreui/react'
import { getCarsService } from '../../services/cars.service'

const Cars = () => {
  const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
  const history = useHistory()
  const [carsData, setCars] = useState()
  const [page, setPage] = useState(currentPage)
  const [pagesQuantity, setPagesQuantity] = useState(1)
  const [isLoading, setIsLoading] = useState(true)

  const pageChange = newPage => {
    currentPage !== newPage && history.push(`/cars?page=${newPage}`)
  }

  const loadCars = async () => {
    const cars = await getCarsService()
    setCars(cars)
    setPagesQuantity(Math.ceil(cars.length/5))
    setIsLoading(false)
  }

  useEffect(() => {
    currentPage !== page && setPage(currentPage)
  }, [currentPage, page])

  useEffect(() => {
    loadCars()
  }, [])
  
  return (
    <CRow>
      <CCol xl={6}>
        <CCard>
          <CCardHeader>
            <h5>Cars</h5>
          </CCardHeader>
          <CCardBody>
          <CDataTable
            items={carsData}
            fields={[
              { key: 'licencePlate', _classes: 'font-weight-bold' },
              'brand', 'model', 'engine' 
            ]}
            hover
            striped
            itemsPerPage={5}
            activePage={page}
            clickableRows
            loading={isLoading}
            onRowClick={(item) => history.push(`/cars/${item.id}`)}
          />
          <CPagination
            activePage={page}
            onActivePageChange={pageChange}
            pages={pagesQuantity}
            doubleArrows={false}
            align="center"
          />
          </CCardBody>
          <CCardFooter className="p-4">
            <CButton onClick={() => history.push('/cars/new')} color="success" block>Create Car</CButton>
          </CCardFooter>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Cars
