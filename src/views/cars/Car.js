import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CInput,
  CInputGroup,
  CRow
} from '@coreui/react'

import { getCarService, createCarService, updateCarService } from '../../services/cars.service'

const Car = ({match}) => {
  const history = useHistory()
  const [car, setCar] = useState({
    licencePlate: '',
    model: '',
    brand: '',
    engine: ''
  })
  const loadCar = async (id) => {
    if (id !== 'new') {
      const data = await getCarService(id)
      setCar(data)
    }
  }

  const handleChange = (event) => {
    setCar({
      ...car,
      [event.target.name]: event.target.value
    })
  }
  
  const submit = async () => {
    if (match.params.id !== 'new') {
      delete car.id
      await updateCarService(match.params.id, car)
    } else {
      await createCarService(car)
    }
    history.push('/cars')
  }

  useEffect(() => {
    loadCar(match.params.id)
  }, [match.params.id])

  return (
    <CRow>
      <CCol lg={6}>
        <CCard>
          <CCardHeader>
            <h5>{ match.params.id !== 'new' ? 'Update a car' : 'Create a car' }</h5>
          </CCardHeader>
          <CCardBody>
            <CForm>
              <CInputGroup className="mb-3">
                <CInput type="text" placeholder="Brand" name="brand" value={car.brand} onChange={handleChange} autoComplete="brand" />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInput type="text" placeholder="Model" name="model" value={car.model} onChange={handleChange} autoComplete="model" />
              </CInputGroup>
              <CInputGroup className="mb-4">
                <CInput type="text" placeholder="Engine" name="engine" value={car.engine} onChange={handleChange} autoComplete="engine" />
              </CInputGroup>
              <CInputGroup className="mb-4">
                <CInput type="text" placeholder="Lincece Plate" name="licencePlate" value={car.licencePlate} onChange={handleChange} autoComplete="licence-plate" />
              </CInputGroup>
            </CForm>
          </CCardBody>
          <CCardFooter className="p-4">
            <CButton onClick={() => {submit()}} color="success" block>Confirm</CButton>
          </CCardFooter>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Car
