import axios from 'axios';
import { get } from './storage.service'

const getCarsService = async () => {
  const resp = await axios.get(
    'https://k00wzxhp0c.execute-api.sa-east-1.amazonaws.com/dev/cars',
    { headers: { 'Authorization': `Bearer ${get('local', 'token')}` } }
  )
  return resp.data
}

const getCarService = async (id) => {
  const resp = await axios.get(
    `https://k00wzxhp0c.execute-api.sa-east-1.amazonaws.com/dev/cars/${id}`,
    { headers: { 'Authorization': `Bearer ${get('local', 'token')}` } }
  )
  return resp.data
}

const createCarService = async (car) => {
  const resp = await axios.post(
    'https://k00wzxhp0c.execute-api.sa-east-1.amazonaws.com/dev/cars',
    car,
    { headers: { 'Authorization': `Bearer ${get('local', 'token')}` } }
  )
  return resp.data
}

const updateCarService = async (id, car) => {
  const resp = await axios.put(
    `https://k00wzxhp0c.execute-api.sa-east-1.amazonaws.com/dev/cars/${id}`,
    car,
    { headers: { 'Authorization': `Bearer ${get('local', 'token')}` } }
  )
  return resp.data
}

export { getCarsService, getCarService, createCarService, updateCarService }