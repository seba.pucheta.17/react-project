const storages = {
  local: localStorage,
  session: sessionStorage
}

const get = (type, key) => {
  return storages[type].getItem(key)
}

const set = (type, key, value) => {
  storages[type].setItem(key, value)
}

export { get, set}