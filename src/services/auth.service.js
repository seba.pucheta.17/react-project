import axios from 'axios';
import { set } from './storage.service'

const signUpService = async (email, password) => {
  const resp = await axios.post(`https://k00wzxhp0c.execute-api.sa-east-1.amazonaws.com/dev/sign-up`, { email, password })
  console.log(resp.data['access-token'])
  set('local', 'token', resp.data['access-token'])
}

const loginService = async (email, password) => {
  const resp = await axios.post(`https://k00wzxhp0c.execute-api.sa-east-1.amazonaws.com/dev/login`, { email, password })
  console.log(resp.data['access-token'])
  set('local', 'token', resp.data['access-token'])
}

export { signUpService, loginService }