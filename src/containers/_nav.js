export default [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    icon: 'cil-applications-settings'
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Cars',
    to: '/cars',
    icon: 'cil-speedometer'
  }
]

